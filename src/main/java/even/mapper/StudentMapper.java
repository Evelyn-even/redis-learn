package even.mapper;

import even.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface StudentMapper {
    int add(Student student);
    Student select(int sno);
    void delete(int sno);
    void updateAge(@Param("sno") int sno, @Param("age") int age);
}
